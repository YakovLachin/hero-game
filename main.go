package main

import (
	"fmt"
	"gitlab.com/YakovLachin/afonasiy-hero"
)

func main() {
	fmt.Println("==============Добро пожаловать в имтитацию сценария Афониного похода============== \n")
	fmt.Println("Получаем Список оружия \n")
	weapons := afonasiy_hero.GetWeaponsForQuest()

	fmt.Println("Создаем склад боеприпасов \n")

	stock   := afonasiy_hero.Stock{}
	stock.Init()

	fmt.Println("На складе присутсвует след оружие: \n")
	for _, weapon := range weapons {
		fmt.Printf("%s \n", weapon)
		stock.AddToStock(weapon)
	}

	fmt.Println("Раставляем слудющих монстров \n")
	monsters := afonasiy_hero.GetMonstersForQuest()
	for _, monster := range monsters {
		fmt.Printf("%s \n", monster)
	}


	fmt.Println("Размышляем над походным рюкзаком для афони\n")
	bag := afonasiy_hero.SelectForTravel(monsters, &stock)

	fmt.Println("Сумка собрана\n")
	fmt.Printf("%s \n", bag)

	fmt.Println("Зовем Афоню\n")
	hero := afonasiy_hero.Hero{}

	hero.SetBag(*bag)
	fmt.Println("Афоня готов для похода\n")

	fmt.Println("Начинаем поход\n")

	for _, monster := range monsters {
		fmt.Printf("На пути монстр: \n %s \n", monster)
		afonasiy_hero.SelectWeapon(monster, &hero)
		fmt.Printf("Афоня Использовал: \n %s \n", hero.Arms)
		fmt.Println("\n")

		if (monster.Healths <= hero.Arms.GetDamage()) {
			fmt.Println("Aфоня  победил!!! \n")
			continue
		}

		fmt.Println("Aфоня погиб ((((((")
		break
	}
}
